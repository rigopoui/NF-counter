#include <stdio.h>
#include <stdlib.h> 
#include <time.h>

void main (int argc, char *argv[]) {
	//our time constants, in seconds
	//A month is defined as 30 days
	//A slightly more accurate YEAR_SECONDS would need to be float, but I
	//want to use integers for simplicity
	unsigned long MINUTE_SECONDS = 60;
	unsigned long HOUR_SECONDS = 3600;
	unsigned long DAY_SECONDS = 86400;
	unsigned long WEEK_SECONDS = 604800;
	unsigned long MONTH_SECONDS = 2592000;
	unsigned long YEAR_SECONDS = 31556926;

	time_t now;
        time(&now);

	struct tm date_now;
	date_now = *localtime(&now);

	// set date as defined from arguments (make sure your arguments are correct)
        struct tm date_args;
	date_args.tm_sec = 0;
	date_args.tm_min = atoi(argv[5]);
	date_args.tm_hour = atoi(argv[4]);
	date_args.tm_mday = atoi(argv[3]);
	date_args.tm_mon = atoi(argv[2]) - 1;
	date_args.tm_year = atoi(argv[1]) - 1900;

	unsigned long seconds_total = (unsigned long)difftime(mktime(&date_now), mktime(&date_args));
	unsigned long streak[7];
	const char *time_names_plural[7];
	const char *time_names_singular[7];

	//calculating the time difference
	streak[0] = seconds_total / YEAR_SECONDS;
	streak[1] = (seconds_total % YEAR_SECONDS) / MONTH_SECONDS;
	streak[2] = (seconds_total % MONTH_SECONDS) / WEEK_SECONDS;
	streak[3] = (seconds_total % WEEK_SECONDS) / DAY_SECONDS;
	streak[4] = (seconds_total % DAY_SECONDS) / HOUR_SECONDS;
	streak[5] = (seconds_total % HOUR_SECONDS) / MINUTE_SECONDS;
	streak[6] = seconds_total % MINUTE_SECONDS;

	//time words in plural
	time_names_plural[0] = "years";
	time_names_plural[1] = "months";
	time_names_plural[2] = "weeks";
	time_names_plural[3] = "days";
	time_names_plural[4] = "hours";
	time_names_plural[5] = "minutes";
	time_names_plural[6] = "seconds";

	//... and singular
	time_names_singular[0] = "year";
	time_names_singular[1] = "month";
	time_names_singular[2] = "week";
	time_names_singular[3] = "day";
	time_names_singular[4] = "hour";
	time_names_singular[5] = "minute";
	time_names_singular[6] = "second";

	printf("Your streak is ");
	for(int i = 0; i < 7; i++){
		if(i == 6){ printf("and ");}
		if(streak[i] != 0){
			if(streak[i] == 1){
				printf("%lu %s ", streak[i], time_names_singular[i]);
			}else {
				printf("%lu %s ", streak[i], time_names_plural[i]);
			}
		}
	}
        printf("(%lu days)", seconds_total / DAY_SECONDS);
	printf("\n");
}
