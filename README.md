# NF-counter
A simple streak counting app in C for counting days/weeks/months/years? since some event occured ;)

Very lightweight, written in C.
Easier to use with a script containing your arguments + an alias.

Here's a usage example:

        \>_ ./counter 1980 6 4 12 43
        Your streak is 37 years 8 months 3 weeks 5 days 19 hours 4 minutes 13 seconds
